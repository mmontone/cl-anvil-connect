(asdf:defsystem #:anvil-connect
  :serial t
  :description "Bindings for Anvil Connect authentication server"
  :author "Mariano Montone"
  :license "Specify license here"
  :components ((:file "package")
               (:file "anvil-connect"))
  :depends-on (:drakma
	       :puri
	       :cl-json
	       :split-sequence
	       :babel))
