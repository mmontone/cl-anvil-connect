(defpackage :anvil-connect.test
  (:use :cl))

(in-package :anvil-connect.test)

(anvil-connect::register-client (list "http://lala.com") :client-name "Triangular Pretzels")
