(in-package #:anvil-connect)

(defun lisp-to-underscores (string)
  (format nil "~{~A~^_~}"
	  (mapcar #'string-downcase
		  (split-sequence:split-sequence #\- string))))

(defun underscores-to-lisp (string)
  (format nil "~{~A~^-~}"
	  (mapcar #'string-upcase
		  (split-sequence:split-sequence #\_ string))))

(defun call-with-anvil-json (function)
  (let ((cl-json:*lisp-identifier-name-to-json*
	 #'lisp-to-underscores)
	(cl-json:*json-identifier-name-to-lisp*
	 #'underscores-to-lisp))
    (funcall function)))

(defmacro with-anvil-json (&body body)
  `(call-with-anvil-json (lambda () ,@body)))      

(defparameter *host* (puri:parse-uri "http://localhost:3000"))

(defun register-client (redirect-uris &rest options)
  "Register a client.

Example:

```(anvil-connect::register-client (list \"https://example.com/callback\") :client-name \"Triangular Pretzels\")```"

  (with-anvil-json
    (let ((uri (puri:merge-uris "/register" *host*))
	  (content (json:encode-json-plist-to-string
		    (append (list :redirect-uris redirect-uris)
			    options))))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :post
	   :accept "application/json"
	   :content-type "application/json"
					;:additional-headers '(("Authorization" . "Bearer asdfasdf"))
	   :content content
	   :parameters nil)
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun read-client (client-id authorization-token)
  "Read a client.

Example:

```(anvil-connect::read-client \"26b62432-04dc-469f-bc32-c567641a71f4\")```"

  (with-anvil-json
    (let ((uri (puri:merge-uris (format nil "/register/~A" client-id) *host*)))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :get
	   :accept "application/json"
	   :additional-headers `(("Authorization" . ,(format nil "Bearer ~A" authorization-token))))
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun update-client (client-id authorization-token &rest options)
  "Register a client.

Example:

```(anvil-connect::update-client \"26b62432-04dc-469f-bc32-c567641a71f4\" :client-name \"Triangular Pretzels\")```"

  (with-anvil-json
    (let ((uri (puri:merge-uris (format nil "/register/~A" client-id) *host*))
	  (content (json:encode-json-plist-to-string
		    options)))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :patch
	   :accept "application/json"
	   :content-type "application/json"
	   :content content
	   :additional-headers `(("Authorization" . ,(format nil "Bearer ~A" authorization-token)))
	   :parameters nil)
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun verify-access-token (access-token authorization-token)
  "Verify an access token"
  (with-anvil-json
    (let ((uri (puri:merge-uris (format nil "/token/verify?access_token=~A" access-token) *host*)))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :get
	   :accept "application/json"
	   :content-type "application/json"
	   :additional-headers `(("Authorization" . ,(format nil "Bearer ~A" authorization-token)))
	   :parameters nil)
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))
  
(defun post-verify-access-token (access-token authorization-token)
  (with-anvil-json
    (let ((uri (puri:merge-uris "/token/verify" *host*))
	  (content (format nil "access_token=~A" access-token)))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :post
	   :accept "application/json"
	   :content-type "application/x-www-form-urlencoded"
	   :additional-headers `(("Authorization" . ,(format nil "Bearer ~A" authorization-token)))
	   :content content
	   :parameters nil)
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun authorize (&key response-type client-id redirect-uri scope)
  "Initiates authentication flows"

  (with-anvil-json
    (let ((uri (puri:merge-uris
		(format nil "/authorize?client_id=~A&response_type=~A&redirect_uri=~A&scope=~A"
			client-id
			response-type
			redirect-uri
			scope)
		*host*)))
      (print uri)
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :get
	   :accept "application/json")
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun read-configuration ()
  "Read configuration"

  (with-anvil-json
    (let ((uri (puri:merge-uris "/.well-known/openid-configuration" *host*)))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :get
	   :accept "application/json"
	   ;;:additional-headers `(("Authorization" . ,(format nil "Bearer ~A" authorization-token)))
	   )
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

(defun userinfo (access-token)
  "Read userinfo"

  (with-anvil-json
    (let ((uri (puri:merge-uris "/userinfo" *host*)))
      (print uri)
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :get
	   :accept "application/json"
	   :additional-headers `(("Authorization" . ,(format nil "Bearer ~A" access-token)))
	   )
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))

#+nil(defun exchange-authorization-code (code redirect-uri authorization-token &optional (grant-type "authorization_code"))
  (with-anvil-json
    (let ((uri (puri:merge-uris "/token" *host*))
	  (content (format nil "grant_type=~A&code=~A&redirect_uri=~A"
			   grant-type
			   code
			   (drakma:url-encode redirect-uri :utf-8))))
      (print content)
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :post
	   :accept "application/json"
	   :content-type ;"application/x-www-form-urlencoded"
	   "application/json"
	   :additional-headers ;`(("Authorization" . ,(format nil "Bearer ~A" authorization-token)))
	   `(("Authorization" . ,(format nil "Basic ~A" (cl-base64:string-to-base64-string "275a914d-3812-490c-893c-5b0de82db4b4:3cef2b67630029976b11"))))
	   :content ;content
	   (json:encode-json-plist-to-string (list :code code
						   :grant-type grant-type
						   :redirect-uri redirect-uri))
	   :parameters nil)
	(values (if (stringp result) result
		    #+nil(alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result)))
		    (babel:octets-to-string result)
		    )
		status)))))

(defun exchange-authorization-code (code redirect-uri client-id client-secret &optional (grant-type "authorization_code"))
  (with-anvil-json
    (let ((uri (puri:merge-uris "/token" *host*))
	  (content (json:encode-json-plist-to-string (list :code code
							   :grant-type grant-type
							   :redirect-uri redirect-uri))))
      (multiple-value-bind (result status)
	  (drakma:http-request
	   (puri:render-uri uri nil)
	   :method :post
	   :accept "application/json"
	   :content-type "application/json"
	   :additional-headers `(("Authorization" . ,(format nil "Basic ~A" (cl-base64:string-to-base64-string (format nil "~A:~A" client-id client-secret)))))
	   :content content
	   :parameters nil)
	(values (if (stringp result) result
		    (alexandria:alist-plist
		     (json:decode-json-from-string
		      (babel:octets-to-string result))))
		status)))))
