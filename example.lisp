(defpackage anvil-connect.example
  (:use :cl))

(in-package :anvil-connect.example)

(require :hunchentoot)

(defparameter *app-host* "http://localhost:9002")
(defparameter *client-id* "275a914d-3812-490c-893c-5b0de82db4b4")
(defparameter *client-secret* "3cef2b67630029976b11")
(defparameter *client-access-token* "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAiLCJzdWIiOiIyNzVhOTE0ZC0zODEyLTQ5MGMtODkzYy01YjBkZTgyZGI0YjQiLCJhdWQiOiIyNzVhOTE0ZC0zODEyLTQ5MGMtODkzYy01YjBkZTgyZGI0YjQiLCJpYXQiOjE0MDQ2MDk5ODgxOTcsInNjb3BlIjoiY2xpZW50In0.aE9Pd21yZmk0eWlucmlTLVdPR2MyWmk2UzUtbU5GMkNVMDFLWFBObVhJUjJvdHd1NXhJcFBXVkQ5WVpJU2R1Q2J6ZWNuTzdpQW9xb0pxYmxRSUhaeWdTa1ZGUGU1dlJLZXF0VzRqN1h3WGFKUmtITHpPay1Jb054aVhoRjlvOEJGblkySDJCZ1F6bWROb25IeWxWSE5qTWZFZnNWNENOd3NhQ1ZLRXY2bDJReEF2U0N5eW9PRXNRRll5QkJmVGRfMm9mcG1KZ1psR0dDLVBldHVLS1k4dUVEMDNLZjF5cnl5bnZRNm1RZEJLcThpeVBTTkR5MnZ0LWlOa3RhU2tqWlpnd3U3alJQVlRKMlExbEZORWFyenhaN2dJZUh3OXVITF9GZXlHd0labHM5SHRMWTNyNjExNEwzdUdTQ2t6QmxPVWZNQkxvLUtBTDczdU94RkU4WktR")

(hunchentoot:define-easy-handler (get-authorized-info :uri "/info")
    ()
  (flet ((authenticate ()
	   (let ((authorize-url (format nil "/authorize?response_type=code&redirect_uri=~A&client_id=~A&scope=openid+profile"
					(drakma:url-encode (format nil "~A/authorize" *app-host*)
							   :utf-8)
					*client-id*)))
	     (hunchentoot:redirect (puri:render-uri
				    (puri:merge-uris authorize-url
						     anvil-connect::*host*)
				    nil)))))
    (let ((access-token #+nil(hunchentoot:get-parameter "access_token")
			(hunchentoot:cookie-in "access_token")))
      (when (not access-token)
	(return-from get-authorized-info (authenticate)))
      (multiple-value-bind (result status)
	  (anvil-connect::verify-access-token access-token *client-access-token*)
	(if (not (equalp status 200))
	    (return-from get-authorized-info (authenticate))
					; else
	    (format nil
		    "Success!! This is authorized info!! ~%~%~A"
		    (anvil-connect::userinfo access-token)))))))

(hunchentoot:define-easy-handler (authorize :uri "/authorize")
    (code)
  (multiple-value-bind (token-info status)
      (anvil-connect::exchange-authorization-code
       code
       (format nil "~A/authorize" *app-host*)
       *client-id*
       *client-secret*)
    (if (equalp status 200)
	#+nil(hunchentoot:redirect (format nil "~A/info?access_token=~A" *app-host* (getf token-info :access-token)))
	(progn
	  (hunchentoot:set-cookie "access_token" :value (getf token-info :access-token))
	  (hunchentoot:redirect (format nil "~A/info" *app-host*)))
	(princ-to-string token-info))))

(hunchentoot:define-easy-handler (google-login :uri "/google") ()
    (cl-who:with-html-output-to-string (html)
      (:a :href (format nil "~Aconnect/google?response_type=code&redirect_uri=~A&client_id=~A&scope=openid+profile"
			anvil-connect::*host*
			(drakma:url-encode (format nil "~A/authorize" *app-host*)
					   :utf-8)	
			*client-id*)
	  (cl-who:str "Login with Google"))))

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 9002))
